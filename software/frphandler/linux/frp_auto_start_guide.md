# frp开机自启动

## frpc

1. 将frpc和frpc.ini文件拷贝到对应目录，设置执行权限：

```shell
cp frpc /usr/bin/frpc
```

```shell
chmod -R 777 /usr/bin/frpc
```

```shell
mkdir /etc/frp
```

```shell
cp frpc.ini /etc/frp/frpc.ini
```

```shell
cp systemd/frpc.service /lib/systemd/system/frpc.service
```

```
vi /lib/systemd/system/frpc.service
```

2. /lib/systemd/system/frpc.service开头添加:

   ```shell
   #!/bin/bash
   ```

（**下面这些是命令不是添加到上面的文件中**）

3. 刷新服务列表：

```shell
systemctl daemon-reload
```



4. 设置开机自启

```shell
systemctl enable frp
```

5. 重启测试

```shell
reboot
```

6. 查看frpc或frps (grep匹配好久都不出来，建议安装htop查看，这样好找一些)

```shell
top
```

## frps

+ 上述操作将frpc换成frps即可
+ 如果要关掉这些程序，kill 对应进程pid 即可


## 参考链接： 

+ [linux下frpc开机自启动](https://blog.csdn.net/yu_pan_love_cat/article/details/120533136)