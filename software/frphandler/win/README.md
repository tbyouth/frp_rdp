## 使用步骤

1. bat文件放到frp程序文件夹下

2. win + r : shell:startup

3. 方案选择

   + 方案1： vps修改里面绝对路径名，将其复制到启动文件夹即可

   + 方案2： runFrpClient.bat创建快捷方式，将快捷方式拖到启动文件夹

4. 说明：closeFrpClient.bat用于手动关闭后台对应frp客户端程序

5. 使用：
   + 启动windows自带的远程控制软件（win + r，输入mstsc）
   + 输入你的服务器地址:目标机器远程桌面映射端口，比如：`192.168.129.1:7001`